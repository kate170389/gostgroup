import java.util.Scanner;

import static java.lang.System.out;

public class Task2 {
    public static void main(String[] args) {
        System.out.println("Please, enter r");
        int r = inputInt(0);
        System.out.println("Please, enter m");
        int m = inputInt(r - 1);
        System.out.println(factorial(m) / (factorial(r) * factorial(m - r)));
    }

    private static int inputInt(int from) {
        Scanner input = new Scanner(System.in);
        while (!input.hasNextInt()) {
            System.out.println("Your number not valid, try again!");
            input.next();
        }
        int data = input.nextInt();
        while (data <= from) {
            out.println("Number must be more then " + from + ", try again!");
            data = inputInt(from);
        }
        return data;
    }

    private static double factorial(int number) {
        return number == 0 ? 1.0 : factorial(number - 1) * number;
    }
}