import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Task3 {
    public static void main(String[] args) {
        System.out.println("Enter text");
        String str = inputString();
        countTheWords(str).entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed()).forEach(System.out::println);
    }

    private static Map<String, Integer> countTheWords(String str) {
        Map<String, Integer> unique = new TreeMap<>();
        for (String string : str.toLowerCase().split("[\\s\\-{}.'?,:;_@=]+")) {
            Integer n = unique.get(string);
            if (n == null) {
                unique.put(string, 1);
            } else
                unique.put(string, n + 1);
        }
        return unique;
    }

    private static String inputString() {
        Scanner input = new Scanner(System.in);
        String str = input.nextLine();
        while ("".equals(str)) {
            System.out.println("Your data not valid, try again!");
            str = input.nextLine();
        }
        return str;
    }
}